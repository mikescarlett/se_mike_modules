# -*- coding: utf-8 -*-
{
    'name': "se_dashboard",

    'summary': """
        Scarlett User Dashboard Built on top of Website rather than CRM for frontend""",

    'description': """
        User dashboard giving quick access to information and statistics.
    """,

    'author': "Michael Hall",
    'website': "http://www.scarlettentertainment.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'website', 'web'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/views.xml',
        'views/templates.xml',
        'views/resources.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}