# -*- coding: utf-8 -*-
from odoo import http
import time
from datetime import datetime, timedelta


class seDashboard(http.Controller):
    @http.route('/userdashboard/', auth='user', website=True)
    def index(self, **kw):
        contacts = http.request.env['res.partner']
        user = http.request.env['res.users'].browse(http.request.env.context.get('uid'))
        print user
        if user.has_group('base.group_erp_manager'):
            #print "Mike is admin"
            admin_access = True
        return http.request.render('se_dashboard.index', {
            'names': contacts.search([('is_company', '=', True), ]),
            'month': time.strftime("%b").upper(),
            'access': admin_access,
        })


# class comrep(http.Controller):
#     @http.route('/se_dashboard/fp/', auth='public', website=True)
#     def index(self, **kw):
#         cnp = http.request.env['commission.account.report']
#         return http.request.render('comrep.index', {
#             'comms': cnp.search([]),
#
#         })


# @http.route('/se_dashboard/se_dashboard/objects/', auth='public')
#     def list(self, **kw):
#         return http.request.render('se_dashboard.listing', {
#             'root': '/se_dashboard/se_dashboard',
#             'objects': http.request.env['se_dashboard.se_dashboard'].search([]),
#         })

#     @http.route('/se_dashboard/se_dashboard/objects/<model("se_dashboard.se_dashboard"):obj>/', auth='public')
#     def object(self, obj, **kw):
#         return http.request.render('se_dashboard.object', {
#             'object': obj
#         })


class JsonTest(http.Controller):
    @http.route('/se_dashboard/jsontest', auth='user', type='json', csrf=False)
    def index(self, **kw):
        user = str(kw.get('input_data'))
        # user_searched = http.request.env['res.users']
        # uname = user_searched.search([('id','=',user)])
        # print uname.name + "hello"
        comms = http.request.env['commission.account.report']
        deals = http.request.env['commission.lines']
        today = time.strftime("%Y-%m-%d")
        dt = datetime.strptime(today, "%Y-%m-%d")
        start = dt - timedelta(days = (dt.weekday() + 1) % 7)
        end = start + timedelta(days=6)
        d1 = start.strftime("%Y-%m-%d 00:00:00")
        d2 = end.strftime("%Y-%m-%d 23:59:59")
        tw_deals = deals.search([('create_date','>=',d1), ('create_date','<=',d2), ('user_id.id','=',user)])
        deals_today = 0
        deals_tw = 0
        for deal in tw_deals:
            deals_tw = deals_tw + deal.amount
            if deal.create_date[:-9] == today:
                deals_today = deals_today + deal.amount
            #print deal.create_date
            #print deal.user_id
            #print deal.amount
        rows = comms.search([('contract_owner.id','=',user)])
        cnp_tm = 0
        output = dict()
        for row in rows:
            #print row.contracted_paid
            cnp_tm = cnp_tm + row.contracted_paid
        output['cnp_tm'] = cnp_tm
        output['currency'] = "&pound;"
        output['deals_today'] = deals_today
        output['deals_tw'] = deals_tw
        #output['current_user'] = uname.name
        #print output['current_user'] + " hello"

        # return str(data['startdate'])
        return {'rows':output,
                }

    @http.route('/se_dashboard/proposals', auth='user', type='json', csrf=False)
    def proposals(self, **kw):
        user = str(kw.get('input_data'))
        #user = http.request.env.context.get('uid')
        #print user
        proposals = http.request.env['sale.order']
        props = proposals.search([('user_id.id','=',user)])
        proposalList = list()
        for p in props:
            if p.date_qualified != False:
                # print p.user_id.name
                # print p.account_id.name
                # print p.date_qualified
                # print p.state
                link = "<a href='/web?#id="+str(p.id)+"&view_type=form&model=sale.order&action=479' target='_blank'><i class='fa fa-pencil-square-o fa-2x'></i></a>"
                proptable = dict()
                proptable['propid'] = p.id
                proptable['link'] = link
                proptable['user_name'] = p.user_id.name
                proptable['account_name'] = p.account_id.name
                proptable['date_qualified'] = p.date_qualified
                proptable['state'] = p.state
                proptable['name'] = p.name
                proptable['event_date'] = p.event_date
                proptable['budget_from'] = p.budget_from
                proptable['budget_to'] = p.budget_to
                proptable['close_date'] = p.close_date
                proptable['client_grading_id'] = p.client_grading_id.name
                proptable['est_commission'] = p.est_commission
                proptable['management_notes'] = p.management_notes
                proposalList.append(proptable)

        return {'proposals': proposalList}

    @http.route('/se_dashboard/people', auth='user', type='json', csrf=False)
    def people(self, **kwargs):
        data = kwargs
        theusers = http.request.env['res.users']
        people = theusers.search([])
        user = http.request.env['res.users'].browse(http.request.env.context.get('uid'))
        ecs = list()
        uids = dict()
        for p in people:
            ecs.append(p.name)
            uids[p.name] = p.id
            # print p.name
        return {'names':ecs, 'ids':uids, 'user_id': user.id}





