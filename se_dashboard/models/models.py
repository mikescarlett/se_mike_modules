# -*- coding: utf-8 -*-

from odoo import models, fields, api

# class se_dashboard(models.Model):
#     _name = 'se_dashboard.se_dashboard'

#     name = fields.Char()
#     value = fields.Integer()
#     value2 = fields.Float(compute="_value_pc", store=True)
#     description = fields.Text()
#
#     @api.depends('value')
#     def _value_pc(self):
#         self.value2 = float(self.value) / 100

# class CustomSalesDashboard(models.Model):
#     _name = "custom.sales.dashboard"
#
#     @api.one
#     def _get_count(self):
#         quotations_count = self.env['sale.order'].search(
#             [('state', '=', 'draft')])
#         orders_count = self.env['sale.order'].search(
#             [('state', '=', 'sales_order')])
#         orders_done_count = self.env['sale.order'].search(
#             [('state', '=', 'done')])
#
#         self.orders_count = len(orders_count)
#         self.quotations_count = len(quotations_count)
#         self.orders_done_count = len(orders_done_count)
#
#     color = fields.Integer(string='Color Index')
#     name = fields.Char(string="Name")
#     orders_count = fields.Integer(compute='_get_count')
#     quotations_count = fields.Integer(compute='_get_count')
#     orders_done_count = fields.Integer(compute='_get_count')
